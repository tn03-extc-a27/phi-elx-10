#ifndef __weatherMonitoringSystem__
#def __weatherMonitoringSystem__
/*
Function 1:
This Function will fetch the data from the weather monitoring system 
*/
typdef struct {
const char[10] Nodeid;
float temperature;
float humidity;
float pressure;
}weatherData;

(weatherData*) getData( const char * instruction);

/*
Function 2:
This Function will configure the weather monitoring system
*/
typedef enum
 { Good,
   Bad,
   Need_Maintainence
 }status_report;

typedef struct {
 struct weatherData* data;
 status_report  health_status;
}weatherSystem;

struct configureFile{
File*  ConfigureFile;
const char[10] Nodeid;
};

(weatherSystem)* configureSystem(ConfigureFile*)


/*
Function 3:
This Function will sent alerts  from the weather monitoring system 
*/

typedef struct {
const char[10] Nodeid;
char[100] Message;
}Alert;

(Alert*) alert();

#endif